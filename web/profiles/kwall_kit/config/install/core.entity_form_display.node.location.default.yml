langcode: en
status: true
dependencies:
  config:
    - field.field.node.location.body
    - field.field.node.location.field_address
    - field.field.node.location.field_geolocation
    - field.field.node.location.field_link
    - field.field.node.location.field_links
    - field.field.node.location.field_media
    - field.field.node.location.field_meta_tags
    - node.type.location
  module:
    - address
    - content_moderation
    - geolocation_address
    - geolocation_google_maps
    - media_library
    - metatag
    - path
    - scheduler
    - text
id: node.location.default
targetEntityType: node
bundle: location
mode: default
content:
  body:
    type: text_textarea_with_summary
    weight: 2
    settings:
      rows: 9
      summary_rows: 3
      placeholder: ''
      show_summary: false
    third_party_settings: {  }
    region: content
  created:
    type: datetime_timestamp
    weight: 5
    region: content
    settings: {  }
    third_party_settings: {  }
  field_address:
    type: address_default
    weight: 11
    region: content
    settings: {  }
    third_party_settings: {  }
  field_geolocation:
    weight: 12
    settings:
      centre:
        client_location:
          weight: 0
          enable: false
          map_center_id: client_location
        fit_bounds:
          enable: true
          settings:
            min_zoom: null
            reset_zoom: false
          weight: 0
          map_center_id: fit_bounds
        fixed_boundaries:
          settings:
            north: ''
            east: ''
            south: ''
            west: ''
          weight: 0
          enable: false
          map_center_id: fixed_boundaries
        freeogeoip:
          weight: 0
          enable: false
          map_center_id: location_plugins
          settings:
            location_option_id: freeogeoip
        ipstack:
          settings:
            access_key: ''
            location_option_id: ipstack
          weight: 0
          enable: false
          map_center_id: location_plugins
        fixed_value:
          settings:
            latitude: null
            longitude: null
            location_option_id: fixed_value
          weight: 0
          enable: false
          map_center_id: location_plugins
      google_map_settings:
        height: 400px
        width: 100%
        type: ROADMAP
        zoom: 17
        maxZoom: 20
        minZoom: 16
        gestureHandling: auto
        map_features:
          control_geocoder:
            enabled: true
            weight: -100
            settings:
              position: TOP_LEFT
              geocoder: google_geocoding_api
              settings:
                label: Address
                description: 'Enter an address to be localized.'
                autocomplete_min_length: 3
                component_restrictions:
                  route: ''
                  locality: ''
                  administrative_area: ''
                  postal_code: ''
                  country: ''
                boundary_restriction:
                  south: ''
                  west: ''
                  north: ''
                  east: ''
          marker_infobubble:
            weight: 0
            settings:
              close_other: 1
              close_button_src: ''
              shadow_style: 0
              padding: 10
              border_radius: 8
              border_width: 2
              border_color: '#039be5'
              background_color: '#fff'
              min_width: null
              max_width: 550
              min_height: null
              max_height: null
              arrow_style: 2
              arrow_position: 30
              arrow_size: 10
              close_button: 0
            enabled: false
          control_zoom:
            enabled: true
            weight: 0
            settings:
              position: RIGHT_CENTER
              behavior: default
              style: LARGE
          map_restriction:
            weight: 0
            settings:
              north: ''
              south: ''
              east: ''
              west: ''
              strict: true
            enabled: false
          map_type_style:
            weight: 0
            settings:
              style: '[]'
            enabled: false
          marker_clusterer:
            weight: 0
            settings:
              image_path: ''
              styles: ''
              zoom_on_click: true
              grid_size: 60
              minimum_cluster_size: 2
              max_zoom: 15
              average_center: false
            enabled: false
          marker_icon:
            weight: 0
            settings:
              marker_icon_path: ''
              anchor:
                x: 0
                'y': 0
              origin:
                x: 0
                'y': 0
              label_origin:
                x: 0
                'y': 0
              size:
                width: null
                height: null
              scaled_size:
                width: null
                height: null
            enabled: false
          marker_infowindow:
            enabled: true
            weight: 0
            settings:
              info_window_solitary: true
              disable_auto_pan: true
              max_width: null
              info_auto_display: false
          control_rotate:
            weight: 0
            settings:
              position: TOP_LEFT
              behavior: default
            enabled: false
          marker_label:
            weight: 0
            settings:
              color: ''
              font_family: ''
              font_size: ''
              font_weight: ''
            enabled: false
          marker_opacity:
            weight: 0
            settings:
              opacity: !!float 1
            enabled: false
          geolocation_marker_scroll_to_id:
            weight: 0
            settings:
              scroll_target_id: ''
            enabled: false
          marker_zoom_to_animate:
            weight: 0
            settings:
              marker_zoom_anchor_id: ''
            enabled: false
          spiderfying:
            weight: 0
            settings:
              spiderfiable_marker_path: /modules/contrib/geolocation/modules/geolocation_google_maps/images/marker-plus.svg
              markersWontMove: true
              keepSpiderfied: true
              nearbyDistance: 20
              circleSpiralSwitchover: 9
              circleFootSeparation: 23
              spiralFootSeparation: 26
              spiralLengthStart: 11
              spiralLengthFactor: 4
              legWeight: 1.5
              markersWontHide: false
              ignoreMapClick: false
            enabled: false
          google_maps_layer_traffic:
            weight: 0
            enabled: false
          control_streetview:
            weight: 0
            settings:
              position: TOP_LEFT
              behavior: default
            enabled: false
          control_maptype:
            enabled: true
            weight: 0
            settings:
              position: RIGHT_BOTTOM
              behavior: default
              style: DEFAULT
          control_recenter:
            enabled: true
            weight: 0
            settings:
              position: TOP_LEFT
          context_popup:
            weight: 0
            settings:
              content:
                value: ''
                format: full_html
            enabled: false
          google_maps_layer_bicycling:
            weight: 0
            enabled: false
          client_location_indicator:
            weight: 0
            enabled: false
          map_disable_tilt:
            weight: 0
            enabled: false
          map_disable_poi:
            weight: 0
            enabled: false
          map_disable_user_interaction:
            weight: 0
            enabled: false
          drawing:
            weight: 0
            settings:
              strokeColor: '#FF0000'
              strokeOpacity: '0.8'
              strokeWeight: '2'
              fillColor: '#FF0000'
              fillOpacity: '0.35'
              polyline: false
              geodesic: false
              polygon: false
            enabled: false
          control_fullscreen:
            weight: 0
            settings:
              position: TOP_LEFT
              behavior: default
            enabled: false
          control_loading_indicator:
            weight: 0
            settings:
              position: TOP_LEFT
              loading_label: Loading
            enabled: false
          control_locate:
            enabled: true
            weight: 0
            settings:
              position: TOP_LEFT
          google_maps_layer_transit:
            weight: 0
            enabled: false
      auto_client_location_marker: '0'
      allow_override_map_settings: 0
      hide_textfield_form: false
      auto_client_location: ''
    third_party_settings:
      geolocation_address:
        enable: true
        address_field: field_address
        geocoder: google_geocoding_api
        settings:
          label: Address
          description: 'Enter an address to be localized.'
          autocomplete_min_length: 3
          component_restrictions:
            route: ''
            locality: ''
            administrative_area: ''
            postal_code: ''
            country: ''
          boundary_restriction:
            south: ''
            west: ''
            north: ''
            east: ''
        sync_mode: manual
        button_position: LEFT_TOP
        direction: duplex
        ignore:
          organization: true
          address-line1: false
          address-line2: false
          locality: false
          administrative-area: false
          postal-code: false
    type: geolocation_googlegeocoder
    region: content
  field_media:
    weight: 3
    settings:
      media_types: {  }
    third_party_settings: {  }
    type: media_library_widget
    region: content
  field_meta_tags:
    weight: 16
    settings:
      sidebar: true
    third_party_settings: {  }
    type: metatag_firehose
    region: content
  langcode:
    type: language_select
    weight: 1
    region: content
    settings:
      include_locked: true
    third_party_settings: {  }
  moderation_state:
    type: moderation_state_default
    weight: 9
    settings: {  }
    region: content
    third_party_settings: {  }
  path:
    type: path
    weight: 8
    region: content
    settings: {  }
    third_party_settings: {  }
  promote:
    type: boolean_checkbox
    settings:
      display_label: true
    weight: 6
    region: content
    third_party_settings: {  }
  publish_on:
    type: datetime_timestamp_no_default
    weight: 14
    region: content
    settings: {  }
    third_party_settings: {  }
  status:
    type: boolean_checkbox
    settings:
      display_label: true
    weight: 10
    region: content
    third_party_settings: {  }
  sticky:
    type: boolean_checkbox
    settings:
      display_label: true
    weight: 7
    region: content
    third_party_settings: {  }
  title:
    type: string_textfield
    weight: 0
    region: content
    settings:
      size: 60
      placeholder: ''
    third_party_settings: {  }
  uid:
    type: entity_reference_autocomplete
    weight: 4
    settings:
      match_operator: CONTAINS
      size: 60
      placeholder: ''
      match_limit: 10
    region: content
    third_party_settings: {  }
  unpublish_on:
    type: datetime_timestamp_no_default
    weight: 13
    region: content
    settings: {  }
    third_party_settings: {  }
  url_redirects:
    weight: 15
    region: content
    settings: {  }
    third_party_settings: {  }
hidden:
  field_link: true
  field_links: true
