<?php

/**
 * Load services definition file.
 */
$settings['container_yamls'][] = __DIR__ . '/services.yml';

/**
 * Include the Pantheon-specific settings file.
 *
 * n.b. The settings.pantheon.php file makes some changes
 *      that affect all environments that this site
 *      exists in.  Always include this file, even in
 *      a local development environment, to ensure that
 *      the site settings remain consistent.
 */
include __DIR__ . "/settings.pantheon.php";

/**
 * Skipping permissions hardening will make scaffolding
 * work better, but will also raise a warning when you
 * install Drupal.
 *
 * https://www.drupal.org/project/drupal/issues/3091285
 */
// $settings['skip_permissions_hardening'] = TRUE;

/**
 * Place the config directory outside of the Drupal root.
 */
$settings['config_sync_directory'] = '../config/default';

/**
 * Apply different settings for each environment.
 */
$current_environment = NULL;

if (defined('PANTHEON_ENVIRONMENT')) {
  if (isset($_ENV['PANTHEON_ENVIRONMENT'])) {
    $current_environment = $_ENV['PANTHEON_ENVIRONMENT'];
  }
}

/*
// Configure Redis
if (defined('PANTHEON_ENVIRONMENT')) {
  // Include the Redis services.yml file. Adjust the path if you installed to a contrib or other subdirectory.
  $settings['container_yamls'][] = __DIR__ . '/redis.services.yml';

  //phpredis is built into the Pantheon application container.
  $settings['redis.connection']['interface'] = 'PhpRedis';
  // These are dynamic variables handled by Pantheon.
  $settings['redis.connection']['host']      = $_ENV['CACHE_HOST'];
  $settings['redis.connection']['port']      = $_ENV['CACHE_PORT'];
  $settings['redis.connection']['password']  = $_ENV['CACHE_PASSWORD'];

  $settings['cache']['default'] = 'cache.backend.redis'; // Use Redis as the default cache.
  $settings['cache_prefix']['default'] = 'pantheon-redis';

  // Set Redis to not get the cache_form (no performance difference).
  $settings['cache']['bins']['form']      = 'cache.backend.database';

  // Use database for discovery to prevent fatal errors
  $settings['cache']['bins']['discovery'] = 'cache.backend.database';

}
*/

if ($current_environment !== NULL) {
  switch ($current_environment) {

    case 'test':

      $config['environment_indicator.indicator']['bg_color'] = '#00385f';
      $config['environment_indicator.indicator']['fg_color'] = '#ffffff';
      $config['environment_indicator.indicator']['name'] = 'Stage';
      $config['media.settings']['iframe_domain'] = 'https://test-purdue-edu.pantheonsite.io';

      // Prevent to send the emails from LOCAL.
      $settings['update_notify_emails'] = [];

      // Will take affect if the module "Reroute emails" is enabled.
      // $config['reroute_email.settings']['enable'] = TRUE;

      break;

    case 'live':

      $config['environment_indicator.indicator']['bg_color'] = '#3a8002';
      $config['environment_indicator.indicator']['fg_color'] = '#ffffff';
      $config['environment_indicator.indicator']['name'] = 'Live';
      // $config['media.settings']['iframe_domain'] = 'https://www.purdue.edu';
      if ( $_SERVER['HTTP_HOST'] === 'contenthubsubdomain.purdue.edu' ) {
        if (isset($_SERVER['HTTP_USER_AGENT_HTTPS']) && $_SERVER['HTTP_USER_AGENT_HTTPS'] === 'ON') {
          $config['media.settings']['iframe_domain'] = 'https://contenthubsubdomain.purdue.edu';
        } else {
          $config['media.settings']['iframe_domain'] = 'http://contenthubsubdomain.purdue.edu';
        }
      } else {
        $config['media.settings']['iframe_domain'] = 'https://live-purdue-edu.pantheonsite.io';
      }

      break;

    default:

      // Environment indicator.
      $config['environment_indicator.indicator']['bg_color'] = '#041a3d';
      $config['environment_indicator.indicator']['fg_color'] = '#ffffff';
      $config['environment_indicator.indicator']['name'] = 'Dev';
      $config['media.settings']['iframe_domain'] = 'https://' . $current_environment . '-purdue-edu.pantheonsite.io';

      // Prevent to send the emails from LOCAL.
      $settings['update_notify_emails'] = [];

      // Will take affect if the module "Reroute emails" is enabled.
      // $config['reroute_email.settings']['enable'] = TRUE;
      // $config['system.performance']['css']['preprocess'] = FALSE;
      // $config['system.performance']['js']['preprocess'] = FALSE;

      // Disable AdvAgg.
      // $config['advagg.settings']['enabled'] = FALSE;

      // Remove it after development. Use a tariff plane instead of.
      // ini_set('max_execution_time', 480);
      // ini_set('memory_limit', '512M');

      // Verbose errors.
      $config['system.logging']['error_level'] = ERROR_REPORTING_DISPLAY_VERBOSE;

  }
}

/**
 * HTTP Client config.
 */
$settings['http_client_config']['timeout'] = 60;

/**
 * Conditionally turn off preprocessing of JS.
 */
/* if (isset($_SERVER['REQUEST_URI']) &&
  strpos($_SERVER['REQUEST_URI'], 'node') !== false &&
  (strpos($_SERVER['REQUEST_URI'], 'edit') !== false ||
    strpos($_SERVER['REQUEST_URI'], 'add') !== false) ) {
  $config['system.performance']['js']['preprocess'] = FALSE;
} */

/**
 * Exclude a few configs from exporting/importing.
 */
// $settings['config_exclude_modules'] = ['devel', 'tb_megamenu', 'webform'];

/**
 * If there is a local settings file, then include it
 */
$local_settings = __DIR__ . "/settings.local.php";
if (file_exists($local_settings)) {
  include $local_settings;
}

// Automatically generated include for settings managed by ddev.
$ddev_settings = dirname(__FILE__) . '/settings.ddev.php';
if (getenv('IS_DDEV_PROJECT') == 'true' && is_readable($ddev_settings)) {
  require $ddev_settings;
}
